import React from 'react';

const someStyle = {
  fontWeight: 'bold'
};

const userOutput = (props) => {
  return (
    <div className='UserOutput'>
      <p>{props.name} like React</p>
      <p style={someStyle}>{props.name} like Angular</p>
    </div>
  );
};

export default userOutput;